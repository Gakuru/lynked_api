import { Request, Response, NextFunction } from "express";
import { HTTP400Error } from "../utils/httpErrors";
import { objectExpression } from "@babel/types";

export const checkSearchParams = (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    if (!req.query.q) {
        throw new HTTP400Error("Missing q parameter");
    } else {
        next();
    }
};

export const checkCarouselParams = (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    if (Object.keys(req.body).length === 0) {
        throw new HTTP400Error("Missing post data");
    } else {
        next();
    }
};

export const checkShopParams = (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    if (Object.keys(req.body).length === 0) {
        throw new HTTP400Error("Missing post data");
    } else {
        next();
    }
};

export const checkShopProductsParams = (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    if (Object.keys(req).length === 0) {
        throw new HTTP400Error("Missing post data");
    } else {
        next();
    }
};

export const checkLandingPageProductsParams = (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    if (Object.keys(req.body).length === 0) {
        throw new HTTP400Error("Missing post data");
    } else {
        next();
    }
};