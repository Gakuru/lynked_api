import carouselRoutes from "../services/carousel/carousel.routes";
import shopRoutes from "../services/shop/shop.routes";
import shopProductsRoutes from "../services/shopProducts/shopProducts.routes";
import landingPageProductsRoutes from "../services/landingPageProducts/landingPageProducts.routes";
import requestsRoutes from "../services/requests/requests.routes";
import userRoutes from "../services/users/user.routes";

export default [
    ...carouselRoutes,
    ...shopRoutes,
    ...shopProductsRoutes,
    ...landingPageProductsRoutes,
    ...requestsRoutes,
    ...userRoutes
];