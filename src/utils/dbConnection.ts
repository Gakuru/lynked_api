import { Pool } from 'pg';

export const conn = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'lynked',
    password: 'docker',
    port: 5432,
});