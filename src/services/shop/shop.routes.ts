import { Request, Response } from "express";
import { checkShopParams } from "../../middleware/checks";
import Shops from "./shop.controller";
import FileUpload from "../fileService/fileUpload.controller";

export default [
  {
    path: "/api/v1/shops",
    method: "get",
    handler: [
      async ({ query }: Request, res: Response) => {
        const result = await new Shops(query).getShops();
        res.status(200).send(result);
      }
    ]
  },
  {
    path: "/api/v1/shops",
    method: "post",
    handler: [
      // checkShopParams,
      async (req: Request, res: Response) => {
        new FileUpload(req, async (parsedFormData: any) => {
          const result = await new Shops(parsedFormData).createShop();
          res.status(200).send(result);
        });
      }
    ]
  },
  {
    path: "/api/v1/shops",
    method: "patch",
    handler: [
      // checkShopParams,
      async (req: Request, res: Response) => {
        new FileUpload(req, async (parsedFormData: any) => {
          const result = await new Shops(parsedFormData).updateShop();
          res.status(200).send(result);
        });
      }
    ]
  },
  {
    path: "/api/v1/shops",
    method: "delete",
    handler: [
      checkShopParams,
      async ({ body }: Request, res: Response) => {
        const result = await new Shops(body).removeShop();
        res.status(200).send(result);
      }
    ]
  }
];