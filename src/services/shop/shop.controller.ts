import uuid from 'uuid/v4';
import { conn } from '../../utils/dbConnection';
import { fileServerURL } from '../../config/urls.json';

interface iShop {
    id?: string,
    uid: string,
    metadata: Object,
    multimedia: Object,
    category: Object,
}

export default class Shops implements iShop {
    id: string = '';
    uri: string = '';
    uid: string = '';
    metadata: Object = {};
    multimedia: Object = {};
    category: Object = {};

    constructor(data: iShop) {
        this.id = data.id ? data.id : uuid();
        this.uid = data.uid;
        this.metadata = data.metadata;
        this.multimedia = data.multimedia;
        this.category = data.category;
    }

    getShops = async () => {
        try {
            const query = `SELECT * FROM get_shops($1,$2)`;
            const values = [this.uid, false];
            let result = await conn.query(query, values);

            const shops = result.rows.map(shop => {
                return {
                    id: shop.id,
                    uid: shop.uid,
                    metadata: shop.metadata,
                    multimedia: shop.multimedia ? shop.multimedia.images.map((image: any) => {
                        return {
                            logoUri: `${fileServerURL}${image.name}`,
                        }
                    })[0] : [],
                    category: shop.category,
                    analysis: {
                        deliveriesCount: 0,
                        starRatingAverage: 0
                    }
                }
            })

            return {
                success: true,
                status: 200,
                message: 'Shops retrieved successfully',
                shops
            };

        } catch (error) {
            console.log(error);
            return {
                success: false,
                status: 400,
                message: 'Shops failed to retrieve',
                error
            };
        }
    }

    createShop = async () => {

        try {
            const query = `CALL insert_shop($1, $2, $3, $4, $5)`;
            const values = [this.id, this.uid, this.metadata, this.category, this.multimedia];
            const results = await conn.query(query, values);

            return {
                success: true,
                status: 200,
                message: 'Shop created successfully',
                shop: {
                    id: this.id,
                    metadata: this.metadata
                }
            }
        } catch (error) {
            return {
                success: false,
                status: 400,
                message: 'Shop failed to create',
                error
            }
        }

    }

    updateShop = async () => {
        try {
            const query = `CALL update_shop($1, $2, $3, $4)`;
            const values = [this.metadata, this.multimedia, this.category, this.id];
            const results = await conn.query(query, values);

            return {
                success: true,
                status: 200,
                message: 'Shop updated successfully',
                shop: {
                    id: this.id,
                    metadata: this.metadata
                }
            }
        } catch (error) {
            return {
                success: false,
                status: 400,
                message: 'Shop failed to update',
                error
            }
        }
    }

    removeShop = async () => {
        try {
            const query = `CALL remove_shop($1)`;
            const values = [this.id];
            const results = await conn.query(query, values);

            return {
                success: true,
                status: 200,
                message: 'Shop deleted successfully',
                shop: {
                    id: this.id,
                    metadata: this.metadata
                }
            }
        } catch (error) {
            return {
                success: false,
                status: 400,
                message: 'Shop failed to delete',
                error
            }
        }
    }

}