import uuid from 'uuid/v4';
import Shop from './shop.controller';

describe("Shop tests", () => {

    const data = {
        id: uuid(),
        uri: 'https://ae01.alicdn.com/kf/HTB1fVODbEGF3KVjSZFv762_nXXaY.png_Q70.png',
        uid: uuid(),
        metadata: { name: 'Test shop name', description: 'Test shop description' },
        multimedia: { logoUri: 'https://ae01.alicdn.com/kf/HTB1fVODbEGF3KVjSZFv762_nXXaY.png_Q70.png' },
        category: { id: uuid(), name: 'Merchandise' },
    };

    const shop = new Shop(data);

    const expectedShopsObject = {
        success: true,
        status: 200,
        message: expect.any(String),
        shops:expect.any(Array)
    }

    const expectedShopStructure = {
        success: true,
        status: 200,
        message: expect.any(String),
        shop: expect.any(Object)
    }

    test('should pass if it creates a shop', async () => {
        const result = await shop.createShop();
        expect(result).toMatchObject(expectedShopStructure);
    });

    xtest('should pass if it retrieves shops', async () => {
        const result = await shop.getShops();
        expect(result).toMatchObject(expectedShopsObject);
    });

    test('should pass if it updates a shop', async () => {
        const result = await shop.updateShop();
        expect(result).toMatchObject(expectedShopStructure);
    });

    test('should pass if it removes a shop', async () => {
        const result = await shop.removeShop();
        expect(result).toMatchObject(expectedShopStructure);
    });

});