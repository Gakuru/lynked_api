import formidable from 'formidable';
import uuid from 'uuid/v4';

interface iUploadFile {
    multimedia?: {},
    formData?: any,
    hasFile?: boolean
}

export default class FileUpload implements iUploadFile {

    multimedia = {
        images: Array(),
        videos: Array(),
    };
    formData = Object();
    hasFile = false;

    constructor(req?: any, callback?: any) {
        let uploadPath = `./src/services/fileService/uploads/`;

        const form = new formidable.IncomingForm();

        form.parse(req);

        form.on('fileBegin', (name, file) => {
            this.hasFile = true;
            const fileId = uuid();
            const fileName = `${fileId}.${file.type.split('/')[1]}`;
            const filePath = `${uploadPath}${fileId}.${file.type.split('/')[1]}`;
            file.path = `${filePath}`;

            this.multimedia.images.push({
                id: fileId,
                name: fileName,
                uri: file.path,
                featured: true
            });
        }).on('field', (name, field) => {
            try {
                this.formData[name] = JSON.parse(field)
            } catch (e) {
                this.formData[name] = field;
            }
        }).on('end', () => {
            if (this.hasFile)
                this.formData.multimedia = this.multimedia;
            callback(this.formData);
        })
    }
}