import uuid from 'uuid/v4';
import { conn } from '../../utils/dbConnection';
import { fileServerURL } from '../../config/urls.json';

interface iRequests {
    id: string,
    requester_id: string,
    provider_id: string,
    service_id: string,
    service_status: string,
    requester_confirms_completion: boolean,
    provider_confirms_completion: boolean,
    requester_rating_score: number,
    provider_rating_score: number,
}

const SERVICE_STATUS = {
    PENDING: 'PENDING',
    ACCEPTED: 'ACCEPTED',
    DECLINED: 'DECLINED',
    COMPLETED: 'COMPLETED',
}

export default class Requests implements iRequests {
    id: string = '';
    requester_id: string = '';
    provider_id: string = '';
    service_id: string = '';
    service_status: string = SERVICE_STATUS.PENDING;
    requester_confirms_completion: boolean = false;
    provider_confirms_completion: boolean = false;
    requester_rating_score: number = 0;
    provider_rating_score: number = 0;

    constructor(data: iRequests) {
        this.id = data.id ? data.id : uuid();
        this.requester_id = data.requester_id;
        this.provider_id = data.provider_id;
        this.service_id = data.service_id;
        this.service_status = data.service_status || this.service_status;
        this.requester_confirms_completion = data.requester_confirms_completion || this.requester_confirms_completion;
        this.provider_confirms_completion = data.provider_confirms_completion || this.provider_confirms_completion;
        this.requester_rating_score = data.requester_rating_score || this.requester_rating_score;
        this.provider_rating_score = data.provider_rating_score || this.provider_rating_score;
    }

    getProviderRequests = async () => {
        try {
            const query = `SELECT * FROM get_provider_requests($1)`;
            const values = [this.provider_id];
            let result = await conn.query(query, values);

            const requests = result.rows;

            return {
                success: true,
                status: 200,
                message: 'Requests retrieved successfully',
                requests
            };

        } catch (error) {
            return {
                success: false,
                status: 400,
                message: 'Requests failed to retrieve',
                error
            };
        }
    }

    getRequesterRequests = async () => {
        try {
            const query = `SELECT * FROM get_requester_requests($1)`;
            const values = [this.requester_id];
            let result = await conn.query(query, values);

            const requests = result.rows;

            return {
                success: true,
                status: 200,
                message: 'Requests retrieved successfully',
                requests
            };

        } catch (error) {
            return {
                success: false,
                status: 400,
                message: 'Requests failed to retrieve',
                error
            };
        }
    }

    createServiceRequest = async () => {
        try {
            const query = `CALL insert_service_request($1, $2, $3, $4, $5, $6, $7, $8, $9)`;
            const values = [
                this.id,
                this.requester_id,
                this.provider_id,
                this.service_id,
                this.service_status,
                this.requester_confirms_completion,
                this.provider_confirms_completion,
                this.requester_rating_score,
                this.provider_rating_score
            ];
            const results = await conn.query(query, values);

            return {
                success: true,
                status: 200,
                message: 'Request created successfully',
                request: {
                    id: this.id,
                }
            }
        } catch (error) {
            console.log(error);
            return {
                success: false,
                status: 400,
                message: 'Request failed to create',
                error
            }
        }

    }

    updateServiceRequest = async () => {
        try {
            const query = `CALL update_service_request($1, $2, $3, $4, $5, $6)`;
            const values = [
                this.id,
                this.service_status,
                this.requester_confirms_completion,
                this.provider_confirms_completion,
                this.requester_rating_score,
                this.provider_rating_score
            ];
            const results = await conn.query(query, values);

            return {
                success: true,
                status: 200,
                message: 'Request updated successfully',
                request: {
                    id: this.id,
                }
            }
        } catch (error) {
            console.log(error);
            return {
                success: false,
                status: 400,
                message: 'Request failed to update',
                error
            }
        }
    }
}