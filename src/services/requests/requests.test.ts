import uuid from 'uuid/v4';
import Request from './requests.controller';

describe("Service request tests", () => {

    const SERVICE_STATUS = {
        PENDING: 'PENDING',
        ACCEPTED: 'ACCEPTED',
        DECLINED: 'DECLINED',
        COMPLETED: 'COMPLETED',
    }

    const data = {
        id: uuid(),
        requester_id: uuid(),
        provider_id: uuid(),
        service_id: uuid(),
        service_status: SERVICE_STATUS.PENDING,
        requester_confirms_completion: false,
        provider_confirms_completion: false,
        requester_rating_score: 0,
        provider_rating_score: 0
    };

    const request = new Request(data);

    const expectedServiceRequestsObject = {
        success: true,
        status: 200,
        message: expect.any(String),
        requests: expect.any(Array)
    }

    const expectedServiceRequestStructure = {
        success: true,
        status: 200,
        message: expect.any(String),
        request: expect.any(Object)
    }

    test('should pass if it creates a service request', async () => {
        const result = await request.createServiceRequest();
        expect(result).toMatchObject(expectedServiceRequestStructure);
    });

    test('should pass if it retrieves requester requests', async () => {
        const result = await request.getRequesterRequests();
        expect(result).toMatchObject(expectedServiceRequestsObject);
    });

    test('should pass if it retrieves provider requests', async () => {
        const result = await request.getProviderRequests();
        expect(result).toMatchObject(expectedServiceRequestsObject);
    });

    test('should pass if it updates a service request', async () => {
        const result = await request.updateServiceRequest();
        expect(result).toMatchObject(expectedServiceRequestStructure);
    });

});