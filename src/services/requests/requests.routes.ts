import { Request, Response } from "express";
import Requests from "./requests.controller";

export default [
  {
    path: "/api/v1/provider/:provider_id/requests",
    method: "get",
    handler: [
      async ({ query }: Request, res: Response) => {
        const result = await new Requests(query).getProviderRequests();
        res.status(200).send(result);
      }
    ]
  },
  {
    path: "/api/v1/requester/:requester_id/requests",
    method: "get",
    handler: [
      async ({ query }: Request, res: Response) => {
        const result = await new Requests(query).getRequesterRequests();
        res.status(200).send(result);
      }
    ]
  },
  {
    path: "/api/v1/requests",
    method: "post",
    handler: [
      async ({ body }: Request, res: Response) => {
        const result = await new Requests(body).createServiceRequest();
        res.status(200).send(result);
      }
    ]
  },
  {
    path: "/api/v1/requests",
    method: "patch",
    handler: [
      async ({ body }: Request, res: Response) => {
        const result = await new Requests(body).updateServiceRequest();
        res.status(200).send(result);
      }
    ]
  }
];