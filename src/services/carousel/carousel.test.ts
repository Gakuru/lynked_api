import { request } from "http";
import uuid from 'uuid/v4';
import Carousel from './carousel.controller';

describe("Carousel tests", () => {

    const data = {
        id: uuid(),
        uri: 'https://ae01.alicdn.com/kf/HTB1fVODbEGF3KVjSZFv762_nXXaY.png_Q70.png'
    };

    const carousel = new Carousel(data);

    const expectedCarouselImagesObject = {
        success: true,
        status: 200,
        message: expect.any(String),
        carousel: expect.any(Array)
    }

    const expectedImageStructure = {
        success: true,
        status: 200,
        message: expect.any(String),
        carouselImage: expect.any(Object)
    }

    test('should pass if it creates a carousel image', async () => {
        const result = await carousel.createCarouselImage();
        expect(result).toMatchObject(expectedImageStructure);
    });

    test('should pass if it retrieves carousel images', async () => {
        const result = await carousel.getCarousel();
        expect(result).toMatchObject(expectedCarouselImagesObject);
    });

    test('should pass if it updates a carousel image', async () => {
        const result = await carousel.updateCarouselImage();
        expect(result).toMatchObject(expectedImageStructure);
    });

    test('should pass if it removes a carousel image', async () => {
        const result = await carousel.removeCarouselImage();
        expect(result).toMatchObject(expectedImageStructure);
    });

});