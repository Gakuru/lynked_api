import uuid from 'uuid/v4';
import { conn } from '../../utils/dbConnection';

interface iCarousel {
    id?: string,
    uri: string,
}

export default class Carousel implements iCarousel {
    id: string = '';
    uri: string = '';

    constructor(data: iCarousel) {
        this.id = data.id ? data.id : uuid();
        this.uri = data.uri;
    }

    getCarousel = async () => {
        try {
            const result = await conn.query(`SELECT * FROM get_carousel_items()`);

            return {
                success: true,
                status: 200,
                message: 'Carousel images retrieved successfully',
                carousel: result.rows
            };
        } catch (error) {
            return {
                success: false,
                status: 400,
                message: 'Carousel images failed to retrieve',
                error
            };
        }
    }

    createCarouselImage = async () => {

        try {
            const query = `CALL insert_carousel_item($1, $2)`;
            const values = [this.id, this.uri];
            const results = await conn.query(query, values);

            return {
                success: true,
                status: 200,
                message: 'Carousel image created successfully',
                carouselImage: {
                    id: this.id,
                    uri: this.uri
                }
            }
        } catch (error) {
            return {
                success: false,
                status: 400,
                message: 'Carousel image failed to create',
                carouselImage: {
                    id: this.id,
                    uri: this.uri
                }
            }
        }
    }

    updateCarouselImage = async () => {
        try {
            const query = `CALL update_carousel_item($1, $2)`;
            const values = [this.id, this.uri,];
            const results = await conn.query(query, values);

            return {
                success: true,
                status: 200,
                message: 'Carousel image updated successfully',
                carouselImage: {
                    id: this.id,
                    uri: this.uri
                }
            }
        } catch (error) {
            return {
                success: false,
                status: 400,
                message: 'Carousel image failed to update',
                carouselImage: {
                    id: this.id,
                    uri: this.uri
                }
            }
        }
    }

    removeCarouselImage = async () => {
        const query = `CALL remove_carousel_item($1)`;
        const values = [this.id];
        try {
            const results = await conn.query(query, values);

            return {
                success: true,
                status: 200,
                message: 'Carousel image deleted successfully',
                carouselImage: {
                    id: this.id,
                    uri: this.uri
                }
            }
        } catch (error) {
            console.log(error);
            return {
                success: false,
                status: 400,
                message: 'Carousel image failed to delete',
                carouselImage: {
                    id: this.id,
                    uri: this.uri
                }
            }
        }
    }

}