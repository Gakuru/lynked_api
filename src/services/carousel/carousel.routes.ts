import { Request, Response } from "express";
import { checkCarouselParams } from "../../middleware/checks";
import Carousel from "./carousel.controller";

export default [
  {
    path: "/api/v1/carousel",
    method: "get",
    handler: [
      async ({ query }: Request, res: Response) => {
        const result = await new Carousel(query).getCarousel();
        res.status(200).send(result);
      }
    ]
  },
  {
    path: "/api/v1/carousel",
    method: "post",
    handler: [
      checkCarouselParams,
      async ({ body }: Request, res: Response) => {
        const result = await new Carousel(body).createCarouselImage();
        res.status(200).send(result);
      }
    ]
  },
  {
    path: "/api/v1/carousel",
    method: "patch",
    handler: [
      checkCarouselParams,
      async ({ body }: Request, res: Response) => {
        const result = await new Carousel(body).updateCarouselImage();
        res.status(200).send(result);
      }
    ]
  },
  {
    path: "/api/v1/carousel",
    method: "delete",
    handler: [
      checkCarouselParams,
      async ({ body }: Request, res: Response) => {
        const result = await new Carousel(body).removeCarouselImage();
        res.status(200).send(result);
      }
    ]
  }
];