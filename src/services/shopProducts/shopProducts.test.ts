import { request } from "http";
import ShopProducts from './shopProducts.controller';
import uuid from 'uuid/v4';

describe("Shop products tests", () => {

    const data = {
        id: uuid(),
        uid: '615d1a1d-ecf3-4438-a65a-9fd942b4fa46',
        metadata: {
            name: 'Mosquito killing bulbs',
            description: 'UV emiiter lights to kill flies, mosquitos and many more insects'
        },
        price: {
            currency: 'KES',
            actualPrice: 400,
            currentSellPrice: 300
        },
        division: {
            id: '55b89e06-78ad-4a35-a932-901e1c036a10',
            name: 'Consumer products',
            key: 'product'
        },
        shop: {
            id: '85ab790b-dacf-4072-90b5-669ca53aba61',
            name: 'My Curio shop'
        },
        category: {
            id: '08eee594-5a55-4b2e-a64f-3ce2e0b5b316',
            name: 'Plumbing'
        },
        stock: {
            minstock: 10,
            stockCount: 30
        },
        statistics: {
            quantitySold: 0
        },
        discount: {
            applyDiscount: true,
            discountPercent: 25
        },
        multimedia: {
            images: [
                {
                    id: '1',
                    uri: 'https://ae01.alicdn.com/kf/HTB1fVODbEGF3KVjSZFv762_nXXaY.png_Q70.png',
                    featured: true
                },
                {
                    id: '2',
                    uri: 'https://ae01.alicdn.com/kf/HTB1fVODbEGF3KVjSZFv762_nXXaY.png_Q70.png',
                    featured: false
                }
            ]
        }
    };

    const shopProducts = new ShopProducts(data);

    const expectedShopProductsObject = {
        success: true,
        status: 200,
        message: expect.any(String),
        shopProducts: expect.any(Array)
    }

    const expectedShopProductsStructure = {
        success: true,
        status: 200,
        message: expect.any(String),
        shopProduct: expect.any(Object)
    }

    test('should pass if it creates a shops\s product', async () => {
        const result = await shopProducts.createProduct();
        expect(result).toMatchObject(expectedShopProductsStructure);
    });

    test('should pass if it retrieves a shop\'s product', async () => {
        const result = await shopProducts.getProducts();
        expect(result).toMatchObject(expectedShopProductsObject);
    });

    test('should pass if it updates a shop\'s product', async () => {
        const result = await shopProducts.updateProduct();
        expect(result).toMatchObject(expectedShopProductsStructure);
    });

    test('should pass if it removes a shop\'s product', async () => {
        const result = await shopProducts.removeProduct();
        expect(result).toMatchObject(expectedShopProductsStructure);
    });

});