import { Request, Response } from 'express';
import { checkShopProductsParams } from '../../middleware/checks';
import ShopProducts from './shopProducts.controller';
import FileUpload from '../fileService/fileUpload.controller'

export default [
  {
    path: "/api/v1/shops/:shopId/products",
    method: "get",
    handler: [
      async ({ params }: Request, res: Response) => {
        const result = await new ShopProducts(params).getProducts(params.shopId);
        res.status(200).send(result);
      }
    ]
  },
  {
    path: "/api/v1/shops/:shopId/products",
    method: "post",
    handler: [
      checkShopProductsParams,
      async (req: Request, res: Response) => {
        new FileUpload(req, async (parsedFormData: any) => {
          const result = await new ShopProducts(parsedFormData).createProduct();
          res.status(200).send(result);
        });
      }
    ]
  },
  {
    path: "/api/v1/shops/:shopId/products",
    method: "patch",
    handler: [
      checkShopProductsParams,
      async (req: Request, res: Response) => {
        new FileUpload(req, async (parsedFormData: any) => {
          const result = await new ShopProducts(parsedFormData).updateProduct();
          res.status(200).send(result);
        });
      }
    ]
  },
  {
    path: "/api/v1/shops/:shopId/products",
    method: "delete",
    handler: [
      checkShopProductsParams,
      async ({ body }: Request, res: Response) => {
        const result = await new ShopProducts(body).removeProduct();
        res.status(200).send(result);
      }
    ]
  }
];