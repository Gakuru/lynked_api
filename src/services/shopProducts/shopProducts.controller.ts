import uuid from 'uuid/v4';
import { conn } from '../../utils/dbConnection';
import { fileServerURL } from '../../config/urls.json';

interface iShopProduct {
    id?: string,
    uid: string,
    shop: Object,
    metadata: Object,
    division: Object,
    category: Object,
    multimedia: Object,
    stock: Object,
    statistics: Object,
    discount: Object,
    price: Object,
}

export default class ShopProducts implements iShopProduct {
    id?: string = '';
    uid: string = '';
    shop: Object = '';
    metadata: Object = {};
    division: Object = {};
    category: Object = {};
    multimedia: Object = {};
    stock: Object = {};
    statistics: Object = {};
    discount: Object = {};
    price: Object = {};

    constructor(data?: iShopProduct | any) {
        this.id = data.id ? data.id : uuid();
        this.uid = data.uid;
        this.shop = data.shop;
        this.metadata = data.metadata;
        this.division = data.division;
        this.category = data.category;
        this.multimedia = data.multimedia;
        this.stock = data.stock;
        this.statistics = data.statistics;
        this.discount = data.discount;
        this.price = data.price;
    }

    getProducts = async (shopId?: string) => {
        try {
            const query = `SELECT * FROM get_shop_products($1)`;
            const values = [shopId];
            let result = await conn.query(query, values);

            return {
                success: true,
                status: 200,
                message: 'Shop products retrieved successfully',
                shopProducts: result.rows.map(shopProduct => {
                    return {
                        id: shopProduct.id,
                        uid: shopProduct.uid,
                        metadata: shopProduct.metadata,
                        shop: shopProduct.shop,
                        price: shopProduct.price,
                        discount: shopProduct.discount,
                        division: shopProduct.division,
                        category: shopProduct.category,
                        multimedia: {
                            images: shopProduct.multimedia ? shopProduct.multimedia.images.map((image: any) => {
                                return {
                                    id: image.id,
                                    uri: `${fileServerURL}${image.name}`,
                                    featured: image.featured,
                                    images: shopProduct.multimedia.images,
                                }
                            }) : [],
                            videos: shopProduct.multimedia ? shopProduct.multimedia.videos.map((video: any) => {
                                return {
                                    id: video.id,
                                    uri: `${fileServerURL}${video.name}`,
                                    featured: video.featured,
                                    videos: shopProduct.multimedia.videos,
                                }
                            }) : [],
                        },
                        stock: shopProduct.stock,
                        statistics: shopProduct.statistics,
                        deleted: shopProduct.deleted,
                        created_at: shopProduct.created_at,
                        updated_at: shopProduct.updated_at
                    }
                })
            };

        } catch (error) {
            return {
                success: false,
                status: 400,
                message: 'Shop products failed to retrieve',
                error
            };
        }
    }

    createProduct = async () => {

        try {
            const query = `CALL insert_shop_product($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)`;
            const values = [this.id, this.uid, this.metadata, this.shop, this.price, this.discount, this.division, this.stock, this.statistics, this.category, this.multimedia];
            const results = await conn.query(query, values);

            return {
                success: true,
                status: 200,
                message: 'Shop product created successfully',
                shopProduct: {
                    id: this.id,
                    metadata: this.metadata
                }
            }
        } catch (error) {
            return {
                success: false,
                status: 400,
                message: 'Shop product failed to create',
                shopProduct: {
                    id: this.id,
                    metadata: this.metadata
                }
            }
        }

    }

    updateProduct = async () => {
        try {
            const query = `CALL update_shop_product($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`;
            const values = [this.metadata, this.shop, this.price, this.discount, this.division, this.stock, this.statistics, this.category, this.multimedia, this.id];
            const results = await conn.query(query, values);

            return {
                success: true,
                status: 200,
                message: 'Shop product updated successfully',
                shopProduct: {
                    id: this.id,
                    metadata: this.metadata
                }
            }
        } catch (error) {
            console.log(error)
            return {
                success: false,
                status: 400,
                message: 'Shop product failed to update',
                error
            }
        }
    }

    removeProduct = async () => {
        try {
            const query = `CALL remove_shop_product($1)`;
            const values = [this.id];
            const results = await conn.query(query, values);

            return {
                success: true,
                status: 200,
                message: 'Shop product deleted successfully',
                shopProduct: {
                    id: this.id,
                    metadata: this.metadata
                }
            }
        } catch (error) {
            return {
                success: false,
                status: 400,
                message: 'Shop product failed to delete',
                shopProduct: {
                    id: this.id,
                    metadata: this.metadata
                }
            }
        }
    }
}