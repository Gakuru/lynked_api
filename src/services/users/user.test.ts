import { request } from "http";
import uuid from 'uuid/v4';
import User from './user.controller';

describe("User tests", () => {

    const data = {
        id: uuid(),
        OAuth: 'GOOGLE',
        OAuthId: uuid(),
        first_name: 'Nelson',
        last_name: 'Gakuru',
        full_name: 'Nelson Gakuru',
        email: 'gakurumaina@gmail.com',
        photo_uri: 'https://ae01.alicdn.com/kf/HTB1fVODbEGF3KVjSZFv762_nXXaY.png_Q70.png',
        mobile_no: '+254725648070',
    };

    const user = new User(data);

    const expectedUserObject = {
        success: true,
        status: 200,
        message: expect.any(String),
        user: expect.any(Array)
    }

    const expectedUserStructure = {
        success: true,
        status: 200,
        message: expect.any(String),
        user: expect.any(Object)
    }

    test('should pass if it creates a user', async () => {
        const result = await user.createUser();
        expect(result).toMatchObject(expectedUserStructure);
    });

    test('should pass if it retrieves user', async () => {
        const result = await user.getUsers();
        expect(result).toMatchObject(expectedUserObject);
    });

    test('should pass if it updates a user', async () => {
        const result = await user.updateUser();
        expect(result).toMatchObject(expectedUserStructure);
    });

    test('should pass if it removes a user', async () => {
        const result = await user.removeUser();
        expect(result).toMatchObject(expectedUserStructure);
    });

});