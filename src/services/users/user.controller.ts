import uuid from 'uuid/v4';
import { conn } from '../../utils/dbConnection';

interface iUser {
    id?: string,
    OAuth: string,
    OAuthId: string,
    first_name: string,
    last_name: string,
    full_name: string,
    email: string,
    photo_uri: string,
    mobile_no: string,
}

export default class User implements iUser {
    id: string = '';
    OAuth: string = '';
    OAuthId: string = '';
    first_name: string = '';
    last_name: string = '';
    full_name: string = '';
    email: string = '';
    photo_uri: string = '';
    mobile_no: string = '';

    USER_CONSTRAINTS = {
        USER_CREATION_CONSTRAINT: 'users_oauth'
    }

    constructor(data: iUser) {
        this.id = data.id || uuid();
        this.OAuth = data.OAuth;
        this.OAuthId = data.OAuthId;
        this.first_name = data.first_name;
        this.last_name = data.last_name;
        this.full_name = data.full_name;
        this.email = data.email;
        this.photo_uri = data.photo_uri;
        this.mobile_no = data.mobile_no || this.mobile_no;
    }

    getUsers = async () => {
        try {
            const result = await conn.query(`SELECT * FROM get_users()`);

            return {
                success: true,
                status: 200,
                message: 'Users retrieved successfully',
                user: result.rows
            };
        } catch (error) {
            return {
                success: false,
                status: 400,
                message: 'Users failed to retrieve',
                error
            };
        }
    }

    private getUserByOAuth = async () => {
        const query = `SELECT * FROM get_user_by_oauth($1,$2)`;
        const values = [this.OAuth, this.OAuthId]
        const results = await conn.query(query, values);
        return results.rows[0];
    }

    createUser = async () => {

        try {
            const query = `CALL insert_user($1, $2, $3, $4, $5, $6, $7, $8, $9)`;
            const values = [
                this.id,
                this.OAuth,
                this.OAuthId,
                this.first_name,
                this.last_name,
                this.full_name,
                this.email,
                this.photo_uri,
                this.mobile_no];

            const results = await conn.query(query, values);

            return {
                success: true,
                status: 200,
                message: 'User created successfully',
                user: {
                    id: this.id,
                    name: this.full_name
                }
            }
        } catch (error) {
            if (error.constraint === this.USER_CONSTRAINTS.USER_CREATION_CONSTRAINT) {
                const user:any = await this.getUserByOAuth();
                return {
                    success: true,
                    status: 200,
                    message: 'User created successfully',
                    user: {
                        id: user.id,
                        name: user.full_name
                    }
                }
            } else {
                return {
                    success: false,
                    status: 400,
                    message: 'User failed to create',
                    user: {
                        id: this.id,
                        name: this.full_name
                    }
                }
            }
        }
    }

    updateUser = async () => {
        try {
            const query = `CALL update_user($1, $2, $3, $4, $5, $6, $7)`;
            const values = [this.id,
            this.first_name,
            this.last_name,
            this.full_name,
            this.email,
            this.photo_uri,
            this.mobile_no];

            const results = await conn.query(query, values);

            return {
                success: true,
                status: 200,
                message: 'User updated successfully',
                user: {
                    id: this.id,
                    name: this.full_name
                }
            }
        } catch (error) {
            console.log(error);
            return {
                success: false,
                status: 400,
                message: 'User failed to update',
                user: {
                    id: this.id,
                    name: this.full_name
                }
            }
        }
    }

    removeUser = async () => {
        const query = `CALL remove_user($1)`;
        const values = [this.id];
        try {
            const results = await conn.query(query, values);

            return {
                success: true,
                status: 200,
                message: 'User deleted successfully',
                user: {
                    id: this.id,
                    name: this.full_name
                }
            }
        } catch (error) {
            console.log(error);
            return {
                success: false,
                status: 400,
                message: 'User failed to delete',
                user: {
                    id: this.id,
                    name: this.full_name
                }
            }
        }
    }

}