import { Request, Response } from "express";
import { checkCarouselParams } from "../../middleware/checks";
import User from "./user.controller";

export default [
  {
    path: "/api/v1/user",
    method: "get",
    handler: [
      async ({ query }: Request, res: Response) => {
        const result = await new User(query).getUsers();
        res.status(200).send(result);
      }
    ]
  },
  {
    path: "/api/v1/user",
    method: "post",
    handler: [
      checkCarouselParams,
      async ({ body }: Request, res: Response) => {
        const result = await new User(body).createUser();
        res.status(200).send(result);
      }
    ]
  },
  {
    path: "/api/v1/user",
    method: "patch",
    handler: [
      checkCarouselParams,
      async ({ body }: Request, res: Response) => {
        const result = await new User(body).updateUser();
        res.status(200).send(result);
      }
    ]
  },
  {
    path: "/api/v1/user",
    method: "delete",
    handler: [
      checkCarouselParams,
      async ({ body }: Request, res: Response) => {
        const result = await new User(body).removeUser();
        res.status(200).send(result);
      }
    ]
  }
];