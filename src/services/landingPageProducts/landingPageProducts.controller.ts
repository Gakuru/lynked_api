import { conn } from '../../utils/dbConnection';
import { fileServerURL } from '../../config/urls.json';

export default class LandingPageProducts {

    getLandingPageProducts = async () => {
        try {
            const query = `SELECT * FROM get_landing_page_products()`;
            let result = await conn.query(query);

            return {
                success: true,
                status: 200,
                message: 'Landing page products retrieved successfully',
                landingPageProducts: result.rows.map(shopProduct => {
                    return {
                        id: shopProduct.id,
                        uid: shopProduct.uid,
                        metadata: shopProduct.metadata,
                        shop: shopProduct.shop,
                        price: shopProduct.price,
                        discount: shopProduct.discount,
                        division: shopProduct.division,
                        category: shopProduct.category,
                        multimedia: {
                            images: shopProduct.multimedia ? shopProduct.multimedia.images.map((image: any) => {
                                return {
                                    id: image.id,
                                    uri: image.name ? `${fileServerURL}/${image.name}` : image.uri,
                                    featured: image.featured
                                }
                            }) : []
                        },
                        stock: shopProduct.stock,
                        statistics: shopProduct.statistics,
                        deleted: shopProduct.deleted,
                        created_at: shopProduct.created_at,
                        updated_at: shopProduct.updated_at
                    }
                })
            };

        } catch (error) {
            return {
                success: false,
                status: 400,
                message: 'Landing page products failed to retrieve',
                error
            };
        }
    }
}