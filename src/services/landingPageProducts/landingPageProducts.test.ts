import LandingPageProducts from './landingPageProducts.controller';

describe("Shop products tests", () => {

    const landingPageProduct = new LandingPageProducts();

    const expectedLandingPageProductsObject = {
        success: true,
        status: 200,
        message: expect.any(String),
        landingPageProducts: expect.any(Object)
    }

    test('should pass if it retrieves landing page products', async () => {
        const result = await landingPageProduct.getLandingPageProducts();
        expect(result).toMatchObject(expectedLandingPageProductsObject);
    });

});