import { Request, Response } from "express";
import { checkLandingPageProductsParams } from "../../middleware/checks";
import LandingPageProducts from "./landingPageProducts.controller";

export default [
  {
    path: "/api/v1/landingpageproducts",
    method: "get",
    handler: [
      async ({ params }: Request, res: Response) => {
        const result = await new LandingPageProducts().getLandingPageProducts();
        res.status(200).send(result);
      }
    ]
  }
];